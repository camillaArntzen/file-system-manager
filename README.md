# 📁 File system manager 📁
<p> The fifth assignment for the Experis Academy Noroff upskill course. This project is a C# console application that can be used to manage and manipulate files.</p>

## Features
<p>The user has five options in the main menu. The first option is to list all the files in the resources directory. After the program has written all the files to the console, the menu will reappear. The second option is to list all the files in the resources directory with an extension. the user will be asked to select extension to search for. The third option is to get info about the dracula file. The program will write the name, size and number of lines in the dracula file to the console. The fourth option is to search for a word in the dracula file. The program will search for the word and write the number of times the word appears to the console. The fifth option is to exit the program. 
All of the operations are logged to the log file in the resources directory. 
</p>

## Setup
<ul>
<li>Clone the repository</li>
<li>Open the project </li>
<li>▶ Click on the green arrow if you're a clicky boi or run it by pressing <b>Ctrl+F5</b></li>
</ul>

### Author
<p>👩‍💻 Camilla Arntzen [https://gitlab.com/camillaArntzen] </p>

### Built with 🧩
<p>Visual Studio Community 2019</p>
<p>.NET Core 3.1 </p>