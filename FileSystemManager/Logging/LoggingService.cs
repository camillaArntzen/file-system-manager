﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSystemManager
{
    //LoggingService Class
    public class LoggingService
    {
        //path for the log file
        public const string path = @"resources\log.txt";

        //Method to write log with correct log text based on the different operations
        public static void logFileInfoOperations(string operation, double time)
        {
            switch (operation)
            {
                case "list-files":
                    WriteLog(createLogText(" Listed all files in directory", time));
                    break;
                case "list-ext":
                    WriteLog(createLogText(" Listed files with extension ", time));
                    break;
                case "file-size":
                    WriteLog(createLogText(" Got the file size of dracula ", time));
                    break;
                case "file-lines":
                    WriteLog(createLogText(" Got the number of lines in dracula ", time));
                    break;
                case "file-name":
                    WriteLog(createLogText(" Got the file name of dracula ", time));
                    break;
            }
        }


        public static void logDraculaSearch(string text, double time)
        {
            WriteLog(createLogText(text, time));
        }

        //creates the text to be logged
        public static string createLogText(string text, double time)
        {
            DateTime timeNow = DateTime.Now; //gets current time
            string logText = timeNow + ": " + text + ". The function took " + time + " ms to execute.";
            
            return logText; 
        }


        //Writes the text to the log file (adds a new line)
        public static void WriteLog(string textToWrite)
        {
            try
            {
                //creates the file if it does not exists, or append to existing
                using (StreamWriter sr = new StreamWriter(path, true))
                {
                    sr.WriteLine(textToWrite); //writes the text as a new line
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

    }


}

