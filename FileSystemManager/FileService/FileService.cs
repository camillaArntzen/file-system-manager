﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace FileSystemManager
{
	//FileService class that reads from files
	public class FileService
	{
		static string resources = @"./resources/";
		static string dracula = @"resources\Dracula.txt";

		private static readonly Stopwatch sw = new Stopwatch(); //stopwatch (for logging)

		//Option 1: Lists all the files in resource folder
		public static void listAllFiles()
        {
           
			Console.WriteLine("Listing all files: ");
			string path = Directory.GetCurrentDirectory() + @"/resources/";
			sw.Start();
			string[] files = Directory.GetFiles(path).Select(file => file.Split("/")[^1]).ToArray();
			
			for(int i=0; i<=files.Length-1; i++)
			{
				Console.WriteLine(files[i]);
			}
			
			sw.Stop();
			LoggingService.logFileInfoOperations("list-files", sw.ElapsedMilliseconds);
			sw.Reset();
			
			return;
        }

		//Option 2: list files with extension
		public static void listWithExtension(string extension)
		{
			Console.WriteLine("Listing files with extension: " + extension + "\n");	
			string path = Directory.GetCurrentDirectory() + @"/resources/";

			try
			{
				sw.Start();
				string[] files = Directory.GetFiles(path, "*." + extension);
				foreach (string file in files)
				{
					Console.WriteLine(Path.GetFileName(file));
				}
				sw.Stop();

			}
			catch (DirectoryNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}
			catch (UnauthorizedAccessException ex)
			{
				Console.WriteLine(ex.Message);
			}
			LoggingService.logFileInfoOperations("list-ext", sw.ElapsedMilliseconds);
			sw.Reset();
			return;
		}

		//Option 3: Dracula file info
		public static void getDraculaFileInfo()
        {
			//Calls the methods to get name, size and number of lines
			getNameDracula();
			getFileSizeDracula();
			getNumLinesDracula();
			return;
        }

		//method that writes the name of the dracula file to the console
		public static void getNameDracula()
        {
			sw.Start();
			string path = Path.Combine(Environment.CurrentDirectory, dracula);
			string fileName = "";

			try
			{
				fileName = Path.GetFileNameWithoutExtension(path);
			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}
			catch (ArgumentException ex)
			{
				Console.WriteLine(ex.Message);
			}
			sw.Stop();
			LoggingService.logFileInfoOperations("file-name", sw.ElapsedMilliseconds);
			sw.Reset();
			Console.WriteLine("The name of the file 'dracula.txt' is:  " + fileName);
			return;
        }


		public static void getFileSizeDracula()
		{
			long fileSize = 0;
			string path = Path.Combine(Environment.CurrentDirectory, dracula);
			sw.Start();
			try
			{
				fileSize = new System.IO.FileInfo(path).Length;
				fileSize = fileSize / 1024;
			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}
			sw.Stop();
			LoggingService.logFileInfoOperations("file-size", sw.ElapsedMilliseconds);
			sw.Reset();
			Console.WriteLine("The size of the file 'dracula.txt' is: " + fileSize + " kB");

		}
		public static void getNumLinesDracula()
        {
			int lines = 0; //variable to count number of lines

			string path = Path.Combine(Environment.CurrentDirectory, dracula); //path to the dracula file
			sw.Start(); //starts the stopwatch (for logging)
			try
			{
				using (StreamReader sr = File.OpenText(path))
				{
					string s;
					while ((s = sr.ReadLine()) != null)
					{
						lines++; //increases the count variable
					}
				}
			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}

			Console.WriteLine("Dracula has " + lines + " lines");

			sw.Stop();
			//calls the log method. with time in ms from stopwatch
			LoggingService.logFileInfoOperations("file-lines", sw.ElapsedMilliseconds);  
			sw.Reset();
	
			return;
        }

		
		//Option 4: Dracula search
		public static void searchDracula(string word)
        {
			sw.Start();

			string path = Path.Combine(Environment.CurrentDirectory, dracula); //path to dracula

			int results = 0;

			try
			{
				//gets all the text from the file
				string draculaText = File.ReadAllText(path).ToLower();
				//counts the number of results that matches the word. sets results to that number
				results = Regex.Matches(draculaText, $@"\b{word}\b").Count;
			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);
			}

			//writes results to console
			Console.WriteLine("The word '" + word + "' was found " + results + " times in Dracula!");

			sw.Stop();
			string log = "The term " + word + " was found " + results + " times. ";
			LoggingService.logDraculaSearch(log, sw.ElapsedMilliseconds); 
			sw.Reset();

		}
	}

}