﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileSystemManager
{
    //Class to deal with the user interaction
    //Provides the user with different options and calls other methods based on the user input. 
    public class OptionsSelect
    {
       
        //this is the main menu that displays the different options and calls methods based on the user input. 
        public static bool MainMenu()
        {
            //lists the options
            Console.WriteLine(" ");
            Console.WriteLine("*************************************************");
            Console.WriteLine("Helo, pls select an option");
            Console.WriteLine("1) List all files");
            Console.WriteLine("2) List files with extension");
            Console.WriteLine("3) Get info about 'Dracula.txt' ");
            Console.WriteLine("4) Search for a word in Dracula.txt");
            Console.WriteLine("5) EXIT");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine("You selected option 1: List all files");
                    //calls the listAllFilesMethod in FileService
                    FileService.listAllFiles();
                    return true;
                case "2":
                    Console.WriteLine("You selected option 2: List files with extension");
                    //Calls the listwithextension method with the extension the user chose as a param
                    FileService.listWithExtension(getExtension());
                    return true;
                case "3":
                    Console.WriteLine("You selected option 3: Get Dracula file info");
                    FileService.getDraculaFileInfo();
                    return true;
                case "4":
                    Console.WriteLine("You selected option 4: Search for a word in dracula");
                    FileService.searchDracula(getSearchWord());
                    return true;
                case "5":
                    Console.WriteLine("Bye!");
                    //stops the program
                    Environment.Exit(0);
                    return false; //return false. user exited. 
                default:
                    //if user input is not a number from 1 to 5. 
                    Console.WriteLine("Illegal input! Must be number from 1-5. Try again.");
                    return true; 
            }

            //method to get the extension to search for from user
            static string getExtension()
            {
                Console.WriteLine("*************************************************");
                Console.WriteLine("Helo, pls select an option");
                Console.WriteLine("1) txt-files");
                Console.WriteLine("2) png-files");
                Console.WriteLine("3) jfif-files");
                Console.WriteLine("4) jpg-files");
                Console.Write("\r\nSelect an option: ");

                string extension = "";

                switch (Console.ReadLine())
                {
                    case "1":
                        extension = "txt";
                        break;
                    case "2":
                        extension = "png";
                        break;
                    case "3":
                        extension = "jfif";
                        break;
                    case "4":
                        extension = "jpg";
                        break;
                    default:
                        Console.WriteLine("Illegal input! Must be a number from 1-4. Try again. ");
                        break;
                }
                return extension;
            }

            //gets the search word from the console
            static string getSearchWord()
            {
                Console.WriteLine("Helo, pls write the word to search for: ");
                string searchWord = Console.ReadLine();
                if (searchWord == "")
                {
                    Console.WriteLine("Invalid input. Pls write something");
                    searchWord = Console.ReadLine();
                }
                return searchWord;
            }


        }

        
    }
}
